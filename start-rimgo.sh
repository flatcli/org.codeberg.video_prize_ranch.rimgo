#!/bin/sh -e

ENVFILE="$HOME/.var/app/org.codeberg.video_prize_ranch.rimgo/config/.env"
if [ -f "$ENVFILE" ]; then
  source "$ENVFILE"
  export PORT
  export ADDRESS
  export IMGUR_CLIENT_ID
else
  echo "No environment file. Check that $ENVFILE exists"
fi

rimgo
